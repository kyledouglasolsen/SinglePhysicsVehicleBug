﻿using UnityEngine;

public class LookAt : MonoBehaviour
{
    private Transform target;

    private void OnEnable()
    {
        target = Camera.main?.transform;
    }

    private void LateUpdate()
    {
        if (target == null)
        {
            return;
        }

        var direction = transform.position - target.position;
        transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
    }
}