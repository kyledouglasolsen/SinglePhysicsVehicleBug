﻿using UnityEngine;

public class TestPlayer : MonoBehaviour, IInputHandler
{
    public Vector3 Velocity;
    public float VelocityMagnitude;

    [SerializeField] private float moveSpeed = 10f;
    [SerializeField] private float turnSpeed = 360f;
    private Rigidbody rBody;

    private void OnEnable()
    {
        rBody = GetComponent<Rigidbody>();
        SinglePhysics.Add(rBody);
    }

    private void OnDisable()
    {
        SinglePhysics.Remove(rBody);
    }

    public void OnInput(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        rBody.AddForce(transform.forward * moveSpeed * deltaTime, ForceMode.VelocityChange);
        rBody.rotation *= Quaternion.Euler(0f, turnSpeed * deltaTime, 0f);

        SinglePhysics.Simulate(rBody, deltaTime);

        Velocity = rBody.velocity;
        VelocityMagnitude = Velocity.magnitude;
    }
}