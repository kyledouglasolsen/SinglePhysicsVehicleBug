﻿using UnityEngine;
using UnityEngine.UI;

public class PrintVelocity : MonoBehaviour
{
    [SerializeField] private Rigidbody rBody;
    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Update()
    {
        text.text = $"{rBody.velocity.magnitude:n3}m/s";
    }
}