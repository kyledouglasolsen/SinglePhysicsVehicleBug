﻿using UnityEngine;

public class Wheel : MonoBehaviour
{
    [SerializeField] private Transform wheelGraphic;
    [Range(0, 20)] [SerializeField] private float naturalFrequency = 10f;
    [Range(0, 3)] [SerializeField] private float dampingRatio = 0.8f;
    [Range(-1, 1)] [SerializeField] private float forceShift = 0.03f;
    [SerializeField] private bool setSuspensionDistance = true;

    private Rigidbody rBody;

    public WheelCollider WheelCollider { get; private set; }

    private void UpdateSpring()
    {
        var spring = WheelCollider.suspensionSpring;

        spring.spring = Mathf.Pow(Mathf.Sqrt(WheelCollider.sprungMass) * naturalFrequency, 2f);
        spring.damper = 2f * dampingRatio * Mathf.Sqrt(spring.spring * WheelCollider.sprungMass);

        WheelCollider.suspensionSpring = spring;

        var wheelRelativeBody = transform.InverseTransformPoint(WheelCollider.transform.position);
        var distance = rBody.centerOfMass.y - wheelRelativeBody.y + WheelCollider.radius;

        WheelCollider.forceAppPointDistance = distance - forceShift;

        if (spring.targetPosition > 0f && setSuspensionDistance)
        {
            WheelCollider.suspensionDistance = WheelCollider.sprungMass * Physics.gravity.magnitude / (spring.targetPosition * spring.spring);
        }
    }

    private void UpdateGraphics()
    {
        if (wheelGraphic == null)
        {
            return;
        }

        Quaternion rot;
        Vector3 pos;
        WheelCollider.GetWorldPose(out pos, out rot);
        var localPosition = rBody.transform.InverseTransformPoint(pos);
        var localRotation = Quaternion.Inverse(rBody.transform.rotation) * rot;
        wheelGraphic.localPosition = localPosition;
        wheelGraphic.localRotation = localRotation;
    }

    private void Update()
    {
        UpdateSpring();
        UpdateGraphics();
    }

    private void Awake()
    {
        WheelCollider = GetComponent<WheelCollider>();
        rBody = GetComponentInParent<Rigidbody>();

        WheelCollider.ConfigureVehicleSubsteps(2f, 5, 12);
    }

    public void SetSteer(float steer)
    {
        WheelCollider.steerAngle = steer;
    }

    public void SetTorque(float throttle)
    {
        WheelCollider.motorTorque = throttle;
    }
}