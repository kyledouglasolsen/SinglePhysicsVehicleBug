﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[Flags]
public enum Keys : uint
{
	None = 0u,
	Move_Left = 1u,
	Move_Right = 2u,
	Move_Forward = 4u,
	Move_Backward = 8u,
	Move_Jump = 16u,
	Use = 32u
}

public static class KeyExtensions
{
	private static readonly Keys[] AllKeys = (Keys[])Enum.GetValues(typeof(Keys));

	public static Vector2 GetMouse()
	{
		return new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
	}

	public static Keys GetKeys()
	{
		var key = Keys.None;

		if (Input.GetKey(KeyCode.W))
		{
			key |= Keys.Move_Forward;
		}

		if (Input.GetKey(KeyCode.A))
		{
			key |= Keys.Move_Left;
		}

		if (Input.GetKey(KeyCode.S))
		{
			key |= Keys.Move_Backward;
		}

		if (Input.GetKey(KeyCode.D))
		{
			key |= Keys.Move_Right;
		}

		if (Input.GetKey(KeyCode.Space))
		{
			key |= Keys.Move_Jump;
		}

		if (Input.GetKeyDown(KeyCode.F))
		{
			key |= Keys.Use;
		}

		return key;
	}

	public static Vector3 AsInputVelocity(this Keys keys)
	{
		var velocity = Vector3.zero;

		if (keys.HasFlag(Keys.Move_Forward))
		{
			velocity += Vector3.forward;
		}

		if (keys.HasFlag(Keys.Move_Left))
		{
			velocity += Vector3.left;
		}

		if (keys.HasFlag(Keys.Move_Backward))
		{
			velocity += Vector3.back;
		}

		if (keys.HasFlag(Keys.Move_Right))
		{
			velocity += Vector3.right;
		}

		return velocity.normalized;
	}

	public static Keys GetRandomKey()
	{
		return AllKeys[Random.Range(0, AllKeys.Length)];
	}

	public static Vector2 GetRandomMouse(float range = 3f)
	{
		return new Vector2(Random.Range(-range, range), Random.Range(-range, range));
	}
}