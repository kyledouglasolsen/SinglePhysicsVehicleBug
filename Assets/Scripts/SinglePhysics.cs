﻿using System.Collections.Generic;
using UnityEngine;

public static class SinglePhysics
{
    private static readonly Dictionary<Rigidbody, RigidbodyReference> RigidbodyLookup = new Dictionary<Rigidbody, RigidbodyReference>();
    private static readonly List<RigidbodyReference> RigidbodyList = new List<RigidbodyReference>();

    static SinglePhysics()
    {
        Physics.autoSimulation = false;
    }

    public static void Add(Rigidbody rigidbody)
    {
        if (!RigidbodyLookup.ContainsKey(rigidbody))
        {
            var reference = new RigidbodyReference(rigidbody);
            RigidbodyList.Add(reference);
            RigidbodyLookup.Add(rigidbody, reference);
        }
    }

    public static void Remove(Rigidbody rigidbody)
    {
        RigidbodyReference reference;

        if (RigidbodyLookup.TryGetValue(rigidbody, out reference))
        {
            RigidbodyList.Remove(reference);
            RigidbodyLookup.Remove(rigidbody);
        }
    }

    public static void Simulate(Rigidbody rigidbody, float deltaTime)
    {
        Pause(rigidbody);
        Physics.Simulate(deltaTime);
        Resume(rigidbody);
    }

    private static void Pause(Rigidbody rigidbody)
    {
        for (var i = 0; i < RigidbodyList.Count; ++i)
        {
            RigidbodyList[i].UpdateData();

            if (RigidbodyList[i].Rigidbody != rigidbody)
            {
                RigidbodyList[i].Rigidbody.isKinematic = true;
            }
        }
    }

    private static void Resume(Rigidbody rigidbody)
    {
        for (var i = 0; i < RigidbodyList.Count; ++i)
        {
            if (RigidbodyList[i].Rigidbody != rigidbody)
            {
                RigidbodyList[i].ApplyData();
            }
        }
    }

    public class RigidbodyReference
    {
        public RigidbodyReference(Rigidbody rigidbody)
        {
            Rigidbody = rigidbody;
        }

        public readonly Rigidbody Rigidbody;
        private PhysicsData physicsData;

        public void UpdateData()
        {
            physicsData = new PhysicsData(Rigidbody);
        }

        public void ApplyData()
        {
            Rigidbody.isKinematic = physicsData.IsKinematic;

            if (!physicsData.IsKinematic)
            {
                Rigidbody.velocity = physicsData.Velocity;
                Rigidbody.angularVelocity = physicsData.AngularVelocity;
            }
        }
    }

    private struct PhysicsData
    {
        public PhysicsData(Rigidbody rigidbody) : this(rigidbody.isKinematic, rigidbody.velocity, rigidbody.angularVelocity)
        {
        }

        public PhysicsData(bool isKinematic, Vector3 velocity, Vector3 angularVelocity)
        {
            IsKinematic = isKinematic;
            Velocity = velocity;
            AngularVelocity = angularVelocity;
        }

        public readonly bool IsKinematic;
        public readonly Vector3 Velocity;
        public readonly Vector3 AngularVelocity;
    }
}