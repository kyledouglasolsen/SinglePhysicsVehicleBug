﻿using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField] private GameObject playerPrefab;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var random = Random.insideUnitCircle * 33f;
            Instantiate(playerPrefab, transform.position + new Vector3(random.x, 0f, random.y), Quaternion.identity);
        }
    }
}