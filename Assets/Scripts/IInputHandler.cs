﻿using UnityEngine;

public interface IInputHandler
{
    void OnInput(int time, Keys keys, Vector2 mouse, float deltaTime);
}