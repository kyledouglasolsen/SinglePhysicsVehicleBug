﻿using System;

public class FixedUpdater
{
	private int lastUpdateTime;
	private Action<int> onFixedUpdate;
	private Action<float> onInterpolateBetweenFixedUpdate;

	public FixedUpdater(int currentTime, int timeStepMilliseconds, Action<int> onFixedUpdate, Action<float> onInterpolateBetweenFixedUpdate = null)
	{
		CurrentTime = lastUpdateTime = currentTime;
		TimeStepMilliseconds = timeStepMilliseconds;
		this.onFixedUpdate = onFixedUpdate;
		this.onInterpolateBetweenFixedUpdate = onInterpolateBetweenFixedUpdate ?? delegate { };
	}

	public int TimeStepMilliseconds { get; }
	public int CurrentTime { get; private set; }

	public bool Update(int now)
	{
		var ranUpdate = false;

		while (CurrentTime < now)
		{
			onFixedUpdate(CurrentTime);
			CurrentTime += TimeStepMilliseconds;
			ranUpdate = true;
		}

		if (ranUpdate)
		{
			lastUpdateTime = now;
		}
		else
		{
			onInterpolateBetweenFixedUpdate((now - lastUpdateTime) / (float)TimeStepMilliseconds);
		}

		return ranUpdate;
	}

	public void SetTime(int now)
	{
		CurrentTime = now;
	}
}