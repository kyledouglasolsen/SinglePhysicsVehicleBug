﻿using UnityEngine;

public class TestCar : MonoBehaviour, IInputHandler
{
    public Vector3 Velocity;
    public float VelocityMagnitude;

    [SerializeField] private float maxSteer = 45f;
    [SerializeField] private float maxTorque = 900f;
    [SerializeField] private Wheel frontLeft = null, frontRight = null, backLeft = null, backRight = null;
    private Rigidbody rBody;

    private void OnEnable()
    {
        rBody = GetComponent<Rigidbody>();
        SinglePhysics.Add(rBody);
    }

    private void OnDisable()
    {
        SinglePhysics.Remove(rBody);
    }

    public void OnInput(int time, Keys keys, Vector2 mouse, float deltaTime)
    {
        var steer = -1f * maxSteer;
        var torque = 1f * maxTorque;

        frontLeft.SetSteer(steer);
        frontRight.SetSteer(steer);

        backLeft.SetTorque(torque);
        backRight.SetTorque(torque);

        SinglePhysics.Simulate(rBody, deltaTime);

        Velocity = rBody.velocity;
        VelocityMagnitude = Velocity.magnitude;
    }
}