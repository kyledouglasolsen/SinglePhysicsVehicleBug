﻿using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField] private int inputsPerSecond = 33;
    private FixedUpdater fixedUpdater;
    private IInputHandler inputHandler;

    private void Awake()
    {
        inputHandler = GetComponent<IInputHandler>();
        
        var deltaTime = 1f / inputsPerSecond;
        var deltaMilliseconds = (int)(deltaTime * 1000f);
        
        fixedUpdater = new FixedUpdater(CurrentTimeInMilliseconds(), deltaMilliseconds, time =>
        {
            inputHandler.OnInput(time, KeyExtensions.GetKeys(), KeyExtensions.GetMouse(), deltaTime);
        });
    }
    
    private void Update()
    {
        fixedUpdater.Update(CurrentTimeInMilliseconds());    
    }

    private static int CurrentTimeInMilliseconds()
    {
        return (int)(Time.time * 1000f);
    }
}